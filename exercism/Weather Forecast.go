package exercism

// CurrentCondition represents the weather condition in string type.
var CurrentCondition string

// CurrentLocation represents the city location in string type.
var CurrentLocation string

// Forecast returns the weather condition for a specific city.
func Forecast(city, condition string) string {
	CurrentLocation, CurrentCondition = city, condition
	return CurrentLocation + " - current weather condition: " + CurrentCondition
}
