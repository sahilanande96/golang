// Problem Statement: Given an integer N, return true it is an Armstrong number otherwise return false.
//
// An Amrstrong number is a number that is equal to the sum of its own digits each raised to the power of the number of digits.
//
// Examples
// Example 1:
// Input:N = 153
// Output:True
// Explanation: 13+53+33 = 1 + 125 + 27 = 153
// Example 2:
// Input:N = 371
// Output: True
// Explanation: 33+53+13 = 27 + 343 + 1 = 371

func armstrongNumber(x int) bool {
	result := 0
	dup := x
	cnt := int(math.Log10(float64(x)) + 1)

	for x > 0 {
		temp := x % 10
		result += int(math.Pow(float64(temp), float64(cnt)))
		x = x / 10
	}
	return result == dup
}
