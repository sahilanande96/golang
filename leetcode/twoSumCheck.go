// Problem Statement: Given an array of integers arr[] and an integer target.
//
// 1st variant: Return YES if there exist two numbers such that their sum is equal to the target. Otherwise, return NO.
//
// 2nd variant: Return indices of the two numbers such that their sum is equal to the target. Otherwise, we will return {-1, -1}.
//
// Note: You are not allowed to use the same element twice. Example: If the target is equal to 6 and num[1] = 3, then nums[1] + nums[1] = target is not a solution.
//
// Examples:
//
// Example 1:
// Input Format: N = 5, arr[] = {2,6,5,8,11}, target = 14
// Result: YES (for 1st variant)
//        [1, 3] (for 2nd variant)
// Explanation: arr[1] + arr[3] = 14. So, the answer is “YES” for the first variant and [1, 3] for 2nd variant.
//
// Example 2:
// Input Format: N = 5, arr[] = {2,6,5,8,11}, target = 15
// Result: NO (for 1st variant)
// 	[-1, -1] (for 2nd variant)
// Explanation: There exist no such two numbers whose sum is equal to the target.

func main() {
	arr := []int{3, 6, 8, 10, 1, 2, 1}
	quickSort(arr, 0, 6)
	fmt.Println("Sorted array is ", arr)
	fmt.Println(twoSumSortArr(arr, 50))
	fmt.Println(twoSumSortArrIndex(arr, 50))

}

func twoSumSortArr(arr []int, target int) bool {
	i, j := 0, len(arr)-1
	if arr[0] > target {
		return false
	}

	for i < j {
		if arr[i]+arr[j] == target {
			return true
		} else if arr[i]+arr[j] > target {
			j--
		} else {
			i++
		}
	}
	return false
}

func twoSumSortArrIndex(arr []int, target int) []int {
	i, j := 0, len(arr)-1
	if arr[0] > target {
		return []int{-1, -1}
	}

	for i < j {
		if arr[i]+arr[j] == target {
			return []int{i, j}
		} else if arr[i]+arr[j] > target {
			j--
		} else {
			i++
		}
	}
	return []int{-1, -1}
}

func quickSort(arr []int, start int, end int) {
	if start < end {
		pivot := quickSortPartition(arr, start, end)

		quickSort(arr, start, pivot-1)
		quickSort(arr, pivot+1, end)
	}
}

func quickSortPartition(arr []int, start int, end int) int {
	pivot := arr[end]
	hold := start
	for i := start; i < end; i++ {
		if arr[i] <= pivot {
			arr[hold], arr[i] = arr[i], arr[hold]
			hold++

		}
	}
	arr[hold], arr[end] = arr[end], arr[hold]
	return hold
}
