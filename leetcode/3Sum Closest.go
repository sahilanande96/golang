// Given an integer array nums of length n and an integer target, find three integers in nums such that the sum is closest to target.

// Return the sum of the three integers.

// You may assume that each input would have exactly one solution.

 

// Example 1:

// Input: nums = [-1,2,1,-4], target = 1
// Output: 2
// Explanation: The sum that is closest to the target is 2. (-1 + 2 + 1 = 2).
// Example 2:

// Input: nums = [0,0,0], target = 1
// Output: 0
// Explanation: The sum that is closest to the target is 0. (0 + 0 + 0 = 0).
 

// Constraints:

// 3 <= nums.length <= 500
// -1000 <= nums[i] <= 1000
// -104 <= target <= 104

func threeSumClosest(nums []int, target int) int {
	sort.Ints(nums)

	closestSum := math.MaxInt64

	for i := 0; i < len(nums)-2; i++ {
		if i > 0 && nums[i] == nums[i-1] {
			continue
		}

		p1, p2 := i+1, len(nums)-1
		for p1 < p2 {
			currentSum := nums[i] + nums[p1] + nums[p2]

			if abs(currentSum-target) < abs(closestSum-target) {
				closestSum = currentSum
			}

			if currentSum < target {
				p1++
			} else if currentSum > target {
				p2--
			} else {
				return currentSum
			}
		}
	}

	return closestSum
}

func abs(a int) int {
	if a < 0 {
		return -a
	}
	return a
}
