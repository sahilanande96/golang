// Problem:  Given an array of size n, sort the array using Merge Sort.
//
// Examples:
//
// Example 1:
// Input: N=5, arr[]={4,2,1,6,7}
// Output: 1,2,4,6,7,
//
//
// Example 2:
// Input: N=7,arr[]={3,2,8,5,1,4,23}
// Output: 1,2,3,4,5,8,23

func mergeSort(nums []int) []int {
	mid := len(nums) / 2

	lfSorted := mergeSort(nums[0:mid])
	rtSorted := mergeSort(nums[mid:len(nums)])

	fmt.Println(lfSorted)
	fmt.Println(rtSorted)

	//return merge(lfSorted, rtSorted)
	return []int{0}
}

func merge(lfSorted []int, rtSorted []int) []int {
	var result []int
	for i := 0; i < len(lfSorted); i++ {
		for j := 0; j < len(rtSorted); j++ {
			if rtSorted[j] < lfSorted[i] {
				result = append(result, rtSorted[j])
			} else {
				result = append(result, lfSorted[i])
			}
		}
	}
	return result
}
