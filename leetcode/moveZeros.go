// Problem Statement: You are given an array of integers, your task is to move all the zeros in the array to the end of the array and move non-negative integers to the front by maintaining their order.
//
// Examples
// Example 1:
// Input:
//  1 ,0 ,2 ,3 ,0 ,4 ,0 ,1
// Output:
//  1 ,2 ,3 ,4 ,1 ,0 ,0 ,0
// Explanation:
//  All the zeros are moved to the end and non-negative integers are moved to front by maintaining order
//
// Example 2:
// Input:
//  1,2,0,1,0,4,0
// Output:
//  1,2,1,4,0,0,0
// Explanation:
//  All the zeros are moved to the end and non-negative integers are moved to front by maintaining order

func moveZeros(arr []int) []int {
	for idx, value := range arr {
		if value == 0 {
			for i := idx; i < len(arr)-1; i++ {
				arr[i], arr[i+1] = arr[i+1], arr[i]
			}
		}
	}
	return arr
}

func moveZerosOptimized(arr []int) []int {
	start := 0
	for idx, value := range arr {
		if value == 0 {
			start = idx
			break
		}

	}
	for i := start; i < len(arr)-1; i++ {
		if arr[i+1] != 0 {
			arr[i+1], arr[start] = arr[start], arr[i+1]
			start++
		}

	}
	return arr
}
