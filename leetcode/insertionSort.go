// Problem Statement: Given an array of N integers, write a program to implement the Insertion sorting algorithm.
//
// Examples:
//
// Example 1:
// Input: N = 6, array[] = {13,46,24,52,20,9}
// Output: 9,13,20,24,46,52
// Explanation:
// After sorting the array is: 9,13,20,24,46,52
//
//
// Example 2:
// Input: N=5, array[] = {5,4,3,2,1}
// Output: 1,2,3,4,5
// Explanation: After sorting the array is: 1,2,3,4,5

func insertionSort(nums []int) []int {
	for i := 1; i < len(nums); i++ {
		for j := i - 1; j >= 0; j-- {
			if nums[j] > nums[i] {
				nums[i], nums[j] = nums[j], nums[i]
				i--
			} else {
				break
			}
		}

	}
	return nums
}
