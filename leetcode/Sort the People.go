// You are given an array of strings names, and an array heights that consists of distinct positive integers. Both arrays are of length n.

// For each index i, names[i] and heights[i] denote the name and height of the ith person.

// Return names sorted in descending order by the people's heights.

 

// Example 1:

// Input: names = ["Mary","John","Emma"], heights = [180,165,170]
// Output: ["Mary","Emma","John"]
// Explanation: Mary is the tallest, followed by Emma and John.
// Example 2:

// Input: names = ["Alice","Bob","Bob"], heights = [155,185,150]
// Output: ["Bob","Alice","Bob"]
// Explanation: The first Bob is the tallest, followed by Alice and the second Bob.
 

// Constraints:

// n == names.length == heights.length
// 1 <= n <= 103
// 1 <= names[i].length <= 20
// 1 <= heights[i] <= 105
// names[i] consists of lower and upper case English letters.
// All the values of heights are distinct.

type person struct {
	name   string
	height int
}

func sortPeople(names []string, heights []int) []string {
	n := len(names)
	people := make([]person, len(names))
	for i := 0; i < n; i++ {
		people[i] = person{names[i], heights[i]}
	}

	// Implement bubble sort to sort by height in descending order
	for i := 0; i < n-1; i++ {
		for j := 0; j < n-i-1; j++ {
			if people[j].height < people[j+1].height {
				// Swap people[j] and people[j+1]
				people[j], people[j+1] = people[j+1], people[j]
			}
		}
	}

	// Extract sorted names
	sortedNames := make([]string, n)
	for i, p := range people {
		sortedNames[i] = p.name
	}

	return sortedNames

}

// //Faster Solution
// func sortPeople(names []string, heights []int) []string {
//     res:=[]string{}
//     mp:=make(map[int]int) //h:index
//     for i:=range heights{
//         mp[heights[i]]=i
//     }
//     sort.Ints(heights)
//     for i:=len(heights)-1;i>=0;i--{
//         name:=names[mp[heights[i]]]
//         res=append(res,name)
//     }
//     return res
// }
