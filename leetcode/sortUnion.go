// Problem Statement: Given two sorted arrays, arr1, and arr2 of size n and m. Find the union of two sorted arrays.
//
// The union of two arrays can be defined as the common and distinct elements in the two arrays.NOTE: Elements in the union should be in ascending order.
//
// Examples
// Example 1:
// Input:
//
// n = 5,m = 5.
// arr1[] = {1,2,3,4,5}
// arr2[] = {2,3,4,4,5}
// Output:
//
//  {1,2,3,4,5}
//
// Explanation:
//
// Common Elements in arr1 and arr2  are:  2,3,4,5
// Distnict Elements in arr1 are : 1
// Distnict Elemennts in arr2 are : No distinct elements.
// Union of arr1 and arr2 is {1,2,3,4,5}
//
// Example 2:
// Input:
//
// n = 10,m = 7.
// arr1[] = {1,2,3,4,5,6,7,8,9,10}
// arr2[] = {2,3,4,4,5,11,12}
// Output:
//  {1,2,3,4,5,6,7,8,9,10,11,12}
// Explanation:
//
// Common Elements in arr1 and arr2  are:  2,3,4,5
// Distnict Elements in arr1 are : 1,6,7,8,9,10
// Distnict Elemennts in arr2 are : 11,12
// Union of arr1 and arr2 is {1,2,3,4,5,6,7,8,9,10,11,12}

func unionArray(arr1 []int, arr2 []int) []int {
	i, j := 0, 0
	var result []int
	for i < len(arr1) && j < len(arr2) {
		if arr1[i] == arr2[j] {
			if len(result) == 0 || result[len(result)-1] != arr1[i] {
				result = append(result, arr1[i])
			}
			i++
			j++
		} else if arr1[i] < arr2[j] {
			if len(result) == 0 || result[len(result)-1] != arr1[i] {
				result = append(result, arr1[i])
			}
			i++
		} else if arr1[i] > arr2[j] {
			if len(result) == 0 || result[len(result)-1] != arr2[j] {
				result = append(result, arr2[j])
			}
			j++
		}
	}

	for i < len(arr1) {
		result = append(result, arr1[i])
		i++
	}
	for j < len(arr2) {
		result = append(result, arr2[j])
		j++
	}

	return result
}
