// Given an integer array nums, return all the triplets [nums[i], nums[j], nums[k]] such that i != j, i != k, and j != k, and nums[i] + nums[j] + nums[k] == 0.

// Notice that the solution set must not contain duplicate triplets.

 

// Example 1:

// Input: nums = [-1,0,1,2,-1,-4]
// Output: [[-1,-1,2],[-1,0,1]]
// Explanation: 
// nums[0] + nums[1] + nums[2] = (-1) + 0 + 1 = 0.
// nums[1] + nums[2] + nums[4] = 0 + 1 + (-1) = 0.
// nums[0] + nums[3] + nums[4] = (-1) + 2 + (-1) = 0.
// The distinct triplets are [-1,0,1] and [-1,-1,2].
// Notice that the order of the output and the order of the triplets does not matter.
// Example 2:

// Input: nums = [0,1,1]
// Output: []
// Explanation: The only possible triplet does not sum up to 0.
// Example 3:

// Input: nums = [0,0,0]
// Output: [[0,0,0]]
// Explanation: The only possible triplet sums up to 0.
 

// Constraints:

// 3 <= nums.length <= 3000
// -105 <= nums[i] <= 105

func threeSum(nums []int) [][]int {
	// Sort the input array first
	sort.Ints(nums)

	// This will hold the result triplets
	result := make([][]int, 0)

	// Iterate over the sorted array
	for i := 0; i < len(nums)-2; i++ {
		// Skip duplicate values for the 'i' index to avoid repeated triplets
		if i > 0 && nums[i] == nums[i-1] {
			continue
		}

		// Use two pointers (l for left, r for right)
		l, r := i+1, len(nums)-1

		// Find two numbers that sum to -nums[i]
		for l < r {
			sum := nums[i] + nums[l] + nums[r]
			if sum == 0 {
				// Found a triplet that sums to 0
				result = append(result, []int{nums[i], nums[l], nums[r]})

				// Skip duplicates for the left pointer
				for l < r && nums[l] == nums[l+1] {
					l++
				}

				// Skip duplicates for the right pointer
				for l < r && nums[r] == nums[r-1] {
					r--
				}

				// Move both pointers inward
				l++
				r--
			} else if sum < 0 {
				// If sum is less than 0, move left pointer to the right
				l++
			} else {
				// If sum is greater than 0, move right pointer to the left
				r--
			}
		}
	}

	// Return the result which contains all unique triplets
	return result
}
