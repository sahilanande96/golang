// Problem Statement: Given an integer N, check whether it is prime or not. A prime number is a number that is only divisible by 1 and itself and the total number of divisors is 2.
//
// Examples
// Example 1:
// Input:N = 2
// Output:True
// Explanation: 2 is a prime number because it has two divisors: 1 and 2 (the number itself).
// Example 2:
// Input:N =10
// Output: False
// Explanation: 10 is not prime, it is a composite number because it has 4 divisors: 1, 2, 5 and 10.

func primeNumber(x int) bool {
	var result []int
	sqrtX := int(math.Sqrt(float64(x)))

	for i := 1; i <= sqrtX; i++ {
		if x%i == 0 {
			result = append(result, i)
			if i != x/i {
				result = append(result, x/i)
			}
		}
	}

	if len(result) == 2 {
		return true
	} else {
		return false
	}
}
