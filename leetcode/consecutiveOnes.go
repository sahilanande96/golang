// Problem Statement: Given an array that contains only 1 and 0 return the count of maximum consecutive ones in the array.
//
// Examples:
//
// Example 1:
//
// Input: prices = {1, 1, 0, 1, 1, 1}
//
// Output: 3
//
// Explanation: There are two consecutive 1’s and three consecutive 1’s in the array out of which maximum is 3.
//
// Input: prices = {1, 0, 1, 1, 0, 1}
//
// Output: 2
//
// Explanation: There are two consecutive 1's in the array.

func findOnes(arr []int) int {
	result, count := 0, 0
	for i := 0; i < len(arr); i++ {
		if arr[i] == 1 {
			count++
		} else {
			if result < count {
				result = count
			}
			count = 0
		}
	}

	if result < count {
		result = count
	}
	return result
}
func findOnes(arr []int) int {
	result, count := 0, 0
	for i := 0; i < len(arr); i++ {
		if arr[i] == 1 {
			count++
		} else {
			if result < count {
				result = count
			}
			count = 0
		}
	}

	if result < count {
		result = count
	}
	return result
}
