// 20. Valid Parentheses
// Easy
// 19.4K
// 1.1K
// Companies
// Given a string s containing just the characters '(', ')', '{', '}', '[' and ']', determine if the input string is valid.
//
// An input string is valid if:
//
// Open brackets must be closed by the same type of brackets.
// Open brackets must be closed in the correct order.
// Every close bracket has a corresponding open bracket of the same type.
//
//
// Example 1:
//
// Input: s = "()"
// Output: true
// Example 2:
//
// Input: s = "()[]{}"
// Output: true
// Example 3:
//
// Input: s = "(]"
// Output: false
//
//
// Constraints:
//
// 1 <= s.length <= 104
// s consists of parentheses only '()[]{}'.


func isValid(s string) bool {
    i := 0
    fmt.Println(len(s))
    if len(s) % 2 != 0 {
        return false
    }
    r := []rune(s)
    for i < len(s) {
        fmt.Printf("r[i] %q and r[i+1] %q ", r[i], r[i+1])
        fmt.Println(rune(r[i]))
        if rune(r[i]) == rune(r[i + 1]) {
            i = i + 2
        }else{
            return false
        }
    }
    return true
}