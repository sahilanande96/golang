// Given an integer array nums, return true if any value appears more than once in the array, otherwise return false.
//
// Example 1:
//
// Input: nums = [1, 2, 3, 3]
//
// Output: true
// Example 2:
//
// Input: nums = [1, 2, 3, 4]
//
// Output: false

func duplicateInteger(arr []int) bool {
	cache := make(map[int]int)

	for _, val := range arr {
		if _, ok := cache[val]; ok {
			return true
		} else {
			cache[val] = 1
		}
	}

	return false
}

