//


func removeDuplicates(nums []int) int {
    // len_nums := len(nums)
    var unique []int
    inResult := make(map[int]bool)
    for _, num := range nums {
        if _, ok := inResult[num]; !ok {
            inResult[num] = true
            unique = append(unique, num)
        }
    }
    fmt.Println(len(unique))
    nums = unique
    return (len(unique))
}

func removeDuplicates(nums []int) int {
    uniqueIdx := 0
    for idx, _ := range nums {
        if idx < (len(nums)-1){
            if nums[idx] == nums[idx+1] {
                continue
            }
            uniqueIdx++
            nums[uniqueIdx] = nums[idx+1]
        }

    }
    return uniqueIdx + 1
}