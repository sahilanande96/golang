// Problem Statement: Given an array of N integers, write a program to implement the Bubble Sorting algorithm.
//
// Examples:
//
// Example 1:
// Input: N = 6, array[] = {13,46,24,52,20,9}
// Output: 9,13,20,24,46,52
// Explanation: After sorting we get 9,13,20,24,46,52
//
//
// Input: N = 5, array[] = {5,4,3,2,1}
// Output: 1,2,3,4,5
// Explanation: After sorting we get 1,2,3,4,5

func bubbleSort(nums []int) []int {
	swap := false
	for i := 0; i < len(nums); i++ {
		for j := 0; j < len(nums)-1; j++ {
			if nums[j] > nums[j+1] {
				nums[j+1], nums[j] = nums[j], nums[j+1]
				swap = true
			}
		}
		if !swap {
			return nums
		}
	}
	return nums
}
