// You are given an array items, where each items[i] = [typei, colori, namei] describes the type, color, and name of the ith item. You are also given a rule represented by two strings, ruleKey and ruleValue.
//
// The ith item is said to match the rule if one of the following is true:
//
// ruleKey == "type" and ruleValue == typei.
// ruleKey == "color" and ruleValue == colori.
// ruleKey == "name" and ruleValue == namei.
// Return the number of items that match the given rule.
//
//
//
// Example 1:
//
// Input: items = [["phone","blue","pixel"],["computer","silver","lenovo"],["phone","gold","iphone"]], ruleKey = "color", ruleValue = "silver"
// Output: 1
// Explanation: There is only one item matching the given rule, which is ["computer","silver","lenovo"].
// Example 2:
//
// Input: items = [["phone","blue","pixel"],["computer","silver","phone"],["phone","gold","iphone"]], ruleKey = "type", ruleValue = "phone"
// Output: 2
// Explanation: There are only two items matching the given rule, which are ["phone","blue","pixel"] and ["phone","gold","iphone"]. Note that the item ["computer","silver","phone"] does not match.
//
//
// Constraints:
//
// 1 <= items.length <= 104
// 1 <= typei.length, colori.length, namei.length, ruleValue.length <= 10
// ruleKey is equal to either "type", "color", or "name".
// All strings consist only of lowercase letters.


func countMatches(items [][]string, ruleKey string, ruleValue string) int {
    var j int
    result := 0
    if ruleKey == "type"{
        j = 0
    } else if ruleKey == "color"{
        j = 1
    } else {
        j = 2
    }

    for i :=0; i < len(items); i++{
        if items[i][j] == ruleValue {
            result ++
        }

    }

    return result

}


// Better Solution
// func countMatches(items [][]string, ruleKey string, ruleValue string) int {
//     	count := 0
// 	switch ruleKey {
// 	case "type":
// 		for _, v := range items {
// 			if ruleValue == v[0] {
// 				count += 1
// 			}
// 		}
// 	case "color":
// 		for _, v := range items {
// 			if ruleValue == v[1] {
// 				count += 1
// 			}
// 		}
// 	case "name":
// 		for _, v := range items {
// 			if ruleValue == v[2] {
// 				count += 1
// 			}
// 		}
// 	}
// 	return count
//
// }