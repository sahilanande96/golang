// Problem statement: Given a number ‘N’, find out the sum of the first N natural numbers.
//
// Examples:
//
// Example 1:
// Input: N=5
// Output: 15
// Explanation: 1+2+3+4+5=15
//
// Example 2:
// Input: N=6
// Output: 21
// Explanation: 1+2+3+4+5+6=15

func sumN(x int) int {
	return x * (x + 1) / 2
}

func sumNRecursive(x int) int {
	if x == 1 {
		return 1
	} else if x == 0 {
		return 0
	} else {
		return x + sumNRecursive(x-1)
	}
}
