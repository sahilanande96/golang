// Given an array, colors, which contains a combination of the following three elements:

//     00 (representing red)

//     11 (representing white)

//     22 (representing blue)

// Sort the array in place so that the elements of the same color are adjacent, with the colors in the order of red, white, and blue. To improve your problem-solving skills, do not utilize the built-in sort function.

// Constraints:

//     1≤1≤ colors.length ≤≤ 300
//     colors[i] can only contain 00s, 11s, or 22s.

package educative_grokking_coding_patterns

func sortColors(colors []int) []int {
	current, start := 0, 0
	end := len(colors) - 1

	for current <= end {
		if colors[current] == 0 {
			colors[current], colors[start] = colors[start], colors[current]
			current++
			start++
		} else if colors[current] == 1 {
			current++
		} else {
			colors[current], colors[end] = colors[end], colors[current]
			end--
		}
	}
	return colors
}
