// Given a sentence, reverse the order of its words without affecting the order of letters within the given word.

// Constraints:

//     The sentence contains English uppercase and lowercase letters, digits, and spaces.

//     1≤1≤ sentence.length ≤104≤104

//     The order of the letters within a word is not to be reversed.

//     Note: The input string may contain leading or trailing spaces or multiple spaces between words. The returned string, however, should only have a single space separating each word. Do not include any extra spaces.


package educative_grokking_coding_patterns

import(
  "strings"  
)


func reverseWords(sentence string) string {
	words := strings.Fields(strings.TrimSpace(sentence))

	start, end := 0, len(words)-1
	for start < end {
		words[start], words[end] = words[end], words[start]
		start++
		end--
	}

	return strings.Join(words, " ")
}
