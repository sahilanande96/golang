// Given an array of integers, nums, and an integer value, target, determine if there are any three integers in nums whose sum is equal to the target, that is, nums[i] + nums[j] + nums[k] == target. Return TRUE if three such integers exist in the array. Otherwise, return FALSE.

//     Note: A valid triplet consists of elements with distinct indexes. This means, for the triplet nums[i], nums[j], and nums[k], i ≠= j, i ≠= k and j ≠= k.

// Constraints:

//     33 ≤≤ nums.length ≤≤ 500500
//     −103−103 ≤≤ nums[i] ≤≤ 103103
//     −103−103 ≤≤ target ≤≤ 10310

// You can edit this code!
// Click here and start typing.
package educative_grokking_coding_patterns

import (
	"sort"
)

func findSumOfThree(nums []int, target int) bool {
	if len(nums) <= 2 {
		return false
	}
	sort.Ints(nums)

	for i := 0; i < len(nums)-2; i++ {
		l, h := i+1, len(nums)-1

		if i > 0 && nums[i] == nums[i-1] {
			continue
		}

		for l < h {
			currentSum := nums[i] + nums[l] + nums[h]
			if currentSum == target {
				return true
			} else if currentSum < target {
				l++
			} else if currentSum > target {
				h--
			}
		}
	}
	return false
}
