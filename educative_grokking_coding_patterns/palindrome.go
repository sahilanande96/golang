// Write a function that takes a string, s, as an input and determines whether or not it is a palindrome.

//     Note: A palindrome is a word, phrase, or sequence of characters that reads the same backward as forward.

// Constraints:

//     1≤1≤ s.length ≤2×105≤2×105
//     The string s will not contain any white space and will only consist of ASCII characters(digits and letters).

package educative_grokking_coding_patterns

func palindrome(s string) bool {
	stringSize := len(s)
	for i := 0; i < stringSize/2; i++ {
		if s[i] != s[stringSize-i-1] {
			return false
		}
	}
	return true
}
