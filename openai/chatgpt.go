package main

import(
    openai "github.com/sashabaranov/go-openai"
    "fmt"
    "bufio"
    "os"
)

func main() {

    for {
        reader := bufio.NewReader(os.Stdin)
        fmt.Print("Ask to chat gpt: ")
        text, err := reader.ReadString('\n')
        if err != nil {
            fmt.Println("Error reading input:", err)
            return
        }

        fmt.Println("Input text:", text)

        c := openai.NewClient("")

        resp, err := c.CreateChatCompletion(
            context.Background(),
            openai.ChatCompletionRequest{
                Model: openai.GPT3Dot5Turbo,
                Messages: []openai.ChatCompletionMessage{
                    {
                        Role:    "user",
                        Content: text,
                    },
                },
            },
        )

        if err != nil {
            panic(err)
        }

        fmt.Println(resp.Choices[0].Message.Content)
        fmt.Println()
    }
}