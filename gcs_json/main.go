package main

import (
	"encoding/json"
	"fmt"
	"github.com/joho/godotenv"
	"os"
	"log"
)

var jsonBytes = []byte(`
{
	"eng_versions":{
		"pipeline": "Value1",
		"batch-server": "Value2"},
	"ai_models":{
		"problem": "Value3",
		"resolve": "Value4"}
}`)

func main () {
   var x map[string]interface{}

    json.Unmarshal([]byte(jsonBytes), &x)
    fmt.Println(x)

    val, ok := x["eng_versions"].(map[string]interface{})
    // If the key exists
    if ok {
        fmt.Println("Eng Versions provided")
        fmt.Println("Pipeline-state should be ", val["pipeline-state"])
        fmt.Println("Rt-job-server should be ", val["rt-job-server"])
    }

    err := godotenv.Load(".env")
    if err != nil {
            log.Fatalf("Error loading environment variables file")
        }

    name := os.Getenv("NAME")
    other := os.Getenv("OTHER")
    fmt.Println(name)
    fmt.Println(other)

}

// Substitute values in the yaml file

