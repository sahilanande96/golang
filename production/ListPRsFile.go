package production

import (
    "golang/fs"
    "golang/github"
    "fmt"
)

func ListPRsFile(filename string, fileExt string, repoOwner string, repo string, page int) (err error) {
    f, err := fs.AppendFile(filename, fileExt)
    if err != nil {
        fmt.Println(err)
        return err
    }

    printStringLabels := fmt.Sprintf("Number,URL,Title,CreatedAt\n")
    _, err = fmt.Fprintf(f, printStringLabels)
    if err != nil {
        fmt.Println(err)
        f.Close()
        return err
    }

    result, err := github.ListPR(repoOwner, repo, page)
    if err != nil {
        fmt.Println(err)
    }
    for i := range result {
        printString := fmt.Sprintf("%d,%s,%s,%s\n", *result[i].Number, *result[i].URL, *result[i].Title, *result[i].CreatedAt)
        _, err := fmt.Fprintf(f, printString)
        if err != nil {
            fmt.Println(err)
            f.Close()
            return err
        }
    }
    err = f.Close()
    if err != nil {
        fmt.Println(err)
        return err
    }

    return nil
}
