package cloudflare

import (
	"fmt"
	"testing"
)

func TestCreateDNSRec(t *testing.T) {
	DNSName := "sahil_test_DNS"
	traefikIP := "203.0.113.50"
	var rec string
	if rec, err := CreateDNSRec(DNSName, traefikIP); err != nil {
		fmt.Printf("%v", err)
		fmt.Errorf("DNS creation failed%v\n", rec)
	}
	fmt.Println("DNS Creation Completed", rec)
}

func TestDeleteDNSRec(t *testing.T) {
	DNSName := "sahil_test_DNS"
	if err := DeleteDNSRec(DNSName); err != nil {
		fmt.Printf("%v", err)
		fmt.Errorf("DNS deletion failed%v\n", DNSName)
	}
	fmt.Println("DNS deletion Completed for ", DNSName)
}

func TestGetDNSRec(t *testing.T) {
	DNSName := "sahil_test_DNS.random.com"
	var rec string
	if rec, err := GetDNSRec(DNSName); err != nil {
		fmt.Printf("%v", err)
		fmt.Errorf("DNS retreival failed%v\n", rec)
	}
	fmt.Println(rec)
	fmt.Println("DNS Retreival Completed ", rec)
}
