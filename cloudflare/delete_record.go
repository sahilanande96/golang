package cloudflare

import (
	"context"
	"fmt"
	cloudflare "github.com/cloudflare/cloudflare-go"
)

func DeleteDNSRec(DNSName string) error {

	ctx := context.Background()

	api, err := cloudflare.New(viper.ViperEnvVar("CLOUDFLARE_GLOBAL_KEY"), viper.ViperEnvVar("CLOUDFLARE_USER_EMAIL"))
	if err != nil {
		fmt.Errorf("error connecting to the Cloudflare server %v", err)
		return err
	}
	fmt.Println(api)

	zoneID, err := api.ZoneIDByName(viper.ViperEnvVar("DOMAIN_NAME"))
	if err != nil {
		return err
	}
	fmt.Println(zoneID)

	err = api.DeleteDNSRecord(ctx, cloudflare.ZoneIdentifier(zoneID), DNSName)
	if err != nil {
		return err
	}
	return nil
}
