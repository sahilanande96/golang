package cloudflare

import (
	"context"
	"fmt"
	"golang/viper"
	cloudflare "github.com/cloudflare/cloudflare-go"
)

func GetDNSRec(DNSName string) (string, error) {

	ctx := context.Background()
	//api, err := cloudflare.NewWithAPIToken(viper.ViperEnvVar("CLOUDFLARE_API_TOKEN"))
	api, err := cloudflare.New(viper.ViperEnvVar("CLOUDFLARE_GLOBAL_KEY"), viper.ViperEnvVar("CLOUDFLARE_USER_EMAIL"))
	if err != nil {
		return "", fmt.Errorf("error connecting to the Cloudflare server %v", err)
	}

	zoneID, err := api.ZoneIDByName(viper.ViperEnvVar("DOMAIN_NAME"))
	if err != nil {
		return "", fmt.Errorf("error retrieving zone_id %v", err)
	}

	DNSRecordResponse, _, err := api.ListDNSRecords(ctx, cloudflare.ZoneIdentifier(zoneID), cloudflare.ListDNSRecordsParams{Name: DNSName})
	if err != nil {
		return "", fmt.Errorf("error getting the DNS record %v", err)
	}
// 	fmt.Println(DNSRecordResponse[0].Name)
	return DNSRecordResponse[0].Name, nil
}
