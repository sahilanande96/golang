package cloudflare

import (
	"context"
	"fmt"
	"golang/viper"
	cloudflare "github.com/cloudflare/cloudflare-go"
)

func BoolPointer(b bool) *bool {
	return &b
}

func CreateDNSRec(DNSName string, traefikIP string) (DNSRec string, err error) {
	ctx := context.Background()

	api, err := cloudflare.NewWithAPIToken(viper.ViperEnvVar("CLOUDFLARE_API_TOKEN"))
	if err != nil {
		return "", fmt.Errorf("error connecting to the Cloudflare server %v", err)
	}

	zoneID, err := api.ZoneIDByName(viper.ViperEnvVar("DOMAIN_NAME"))
	if err != nil {
		return "", fmt.Errorf("error retrieving zone_id %v", err)
	}

	fmt.Println("Creating DNS record")
	DNSRecordResponse, err := api.CreateDNSRecord(ctx, cloudflare.ZoneIdentifier(zoneID), cloudflare.CreateDNSRecordParams{
		Type:    "A",
		Name:    DNSName,   //"sahil_test_4"
		Content: traefikIP, //"203.0.113.50"
		ZoneID:  zoneID,
		TTL:     1,
		Proxied: BoolPointer(true),
	})
	if err != nil {
		return "", fmt.Errorf("error creating a DNS record %v", err)
	}
	//fmt.Println(DNSRecordResponse)
	return DNSRecordResponse.Result.Name, nil
}
