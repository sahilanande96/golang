package bufio

import (
    "fmt"
	"bufio"
	"os"
	"strings"
)

func ReadLine() (stdout string, err error) {
	reader := bufio.NewReader(os.Stdin)
	// ReadString will block until the delimiter is entered
	fmt.Print("Enter text: ")
	input, err := reader.ReadString('\n')
	if err != nil {
		fmt.Println("An error occurred while reading input from stdin. Please try again", err)
		return "", err
	}

	input = strings.TrimSuffix(input, "\n")
    return input, nil
}

func ReadMultiLine() (sentence string, err error) {
	scanner := bufio.NewScanner(os.Stdin)
	for {
		fmt.Print("Enter Text: ")
		scanner.Scan()
		text := scanner.Text()
		if len(text) != 0 {
			sentence = sentence + text + "\n"
		} else {
			break
		}

	}

	if scanner.Err() != nil {
		fmt.Println("Error: ", scanner.Err())
		return "", scanner.Err()
	}

	return sentence, nil
}