package main

import (
	"cloud.google.com/go/storage"
	"context"
	"fmt"
	"google.golang.org/api/iterator"
	"log"
	"os"
	"regexp"
)

type JsonData struct {
	PatientName, PatientID, SourceFileName, SourceFileID, FileExt string
}

func main() {
	ctx := context.Background()
	cli, err := storage.NewClient(ctx)
	if err != nil {
		log.Fatal(err)
	}
	defer cli.Close()

	f, err := os.OpenFile("test.json", os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0600)
	if err != nil {
		panic(err)
	}

	defer f.Close()

	bkt := cli.Bucket("data-export-mendel-lives-test")
	it := bkt.Objects(ctx, &storage.Query{
		Prefix:    "Documents/",
		Delimiter: "",
	})
	re := regexp.MustCompile("(.*)\\/(.*) \\((.*?)\\)\\/(.*)- (.*)\\.(.*)")
	for {
		attrs, err := it.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			log.Fatal(err)
		}
		parts := re.FindStringSubmatch(attrs.Name)
		fmt.Println(parts)
		//for i, _ := range parts {
		//	fmt.Println(parts[i])
		//}
		data := JsonData{
			PatientName:    parts[1],
			PatientID:      parts[2],
			SourceFileName: parts[3],
			SourceFileID:   parts[4],
			FileExt:        parts[5],
		}
		fmt.Println(data)
		//file, _ := json.MarshalIndent(data, "", " ")
		////fmt.Println(file)
		//dst := &bytes.Buffer{}
		//if err := json.Compact(dst, []byte(file)); err != nil {
		//	panic(err)
		//}
		//
		//if _, err = f.WriteString(dst.String()); err != nil {
		//	panic(err)
		//}
		//if _, err = f.WriteString("\n"); err != nil {
		//	panic(err)
		//}
	}
}