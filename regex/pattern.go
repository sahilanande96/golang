package regex
import (
    "fmt"
    "regexp"
)
func main() {
    pattern, compileErr := regexp.Compile("[A-z]ork")
    correctAnswer := "Yes, I love new york city"
    question := "Do you love new york city?"
    wrongAnswer := "I love dogs"
    if compileErr == nil {
        fmt.Println("Question:", pattern.MatchString(question))
        fmt.Println("Correct Answer:", pattern.MatchString(correctAnswer))
        fmt.Println("Wrong Answer:", pattern.MatchString(wrongAnswer))
    } else {
        fmt.Println("Error:", compileErr)
    }
}
