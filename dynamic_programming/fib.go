package dynamic_programming
// package main


import (
    "fmt"
    "log"
    "time"
)

var memo = make(map[int]int)

// for tracking program's execution time
func trackTime(start time.Time) {
	elapsed := time.Since(start)
	log.Printf("---> It took: %s", elapsed)
}

// O(2^n) Time Complexity
func fibRecursive(n int) int {
    if n <= 2 {
        return 1
        }

    return fibRecursive(n -1) + fibRecursive(n - 2)
}

// O(n) Time Complexity
func fibMemoized(n int) int  {
    if n <= 2 {
        return 1
        }
    if value, exists := memo[n]; exists {
        return value
    }

    memo[n] = fibMemoized(n - 1) + fibMemoized(n- 2)
    return(memo[n])
}

// func main () {
//     defer trackTime(time.Now())
//     fmt.Println(fibMemoized(50))
// }