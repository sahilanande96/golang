// The Tribonacci sequence Tn is defined as follows:
//
// T0 = 0, T1 = 1, T2 = 1, and Tn+3 = Tn + Tn+1 + Tn+2 for n >= 0.
//
// Given n, return the value of Tn.
//
//
//
// Example 1:
//
// Input: n = 4
// Output: 4
// Explanation:
// T_3 = 0 + 1 + 1 = 2
// T_4 = 1 + 1 + 2 = 4
// Example 2:
//
// Input: n = 25
// Output: 1389537
//
//
// Constraints:
//
// 0 <= n <= 37
// The answer is guaranteed to fit within a 32-bit integer, ie. answer <= 2^31 - 1.

package main

import (
    "fmt"
    "log"
    "time"
)

var memo = make(map[int]int)

// for tracking program's execution time
func trackTime(start time.Time) {
	elapsed := time.Since(start)
	log.Printf("---> It took: %s", elapsed)
}

func tribonacciRecursive(n int) int {
    if n == 0 {
        return  0
    }

    if n == 1 || n == 2 {
        return 1
    }

    return (tribonacciRecursive(n-1) + tribonacciRecursive(n-2) + tribonacciRecursive(n-3))
}

func tribonacciMemo(n int) int {
    if value, exists := memo[n]; exists {
        return value
    }

    if n == 0 {
        return  0
    }

    if n == 1 || n == 2 {
        return 1
    }

    memo[n] = tribonacciMemo(n-1) + tribonacciMemo(n-2) + tribonacciMemo(n-3)
    return memo[n]
}

// func main(){
//     defer trackTime(time.Now())
//     fmt.Println("Result is ", tribonacciMemo(36))
// }
