// So say that you're a traveler on a two dimensional grid, you begin in the top left corner, and your goal is to travel to the bottom right corner of that grid. And the rule is you can only move down or to the right, that means you can't move up or to the left, and you definitely can't move to AGL. That being said, In how many ways can you travel to the goal? That is how many ways can you travel to the bottom rate if you had a grid of dimensions, m by n.

package dynamic_programming
// package main

import (
    "fmt"
    "time"
    "log"
)

type memoKey struct {
    m int
    n int
}

var memo = map[memoKey]int{}

// for tracking program's execution time
func trackTime(start time.Time) {
	elapsed := time.Since(start)
	log.Printf("---> It took: %s", elapsed)
}

// O(2^(m+n)) Time Complexity and O(n+m) Space complexity
func gridTravelerRecursive(m int, n int) int {
    if m == 0 || n == 0 {
        return 0
    }
    if m == 1 && n == 1 {
        return 1
    }

    return gridTravelerRecursive(m-1,n) + gridTravelerRecursive(m, n-1)
}


func gridTravelerMemoized(m int, n int) int {
    if m == 0 || n == 0 {
        return 0
    }
    if m == 1 && n == 1 {
        return 1
    }
    if value, exists := memo[memoKey{m,n}]; exists {
        return value
    }

    memo[memoKey{m,n}] = gridTravelerMemoized(m-1,n) + gridTravelerMemoized(m, n-1)
    return memo[memoKey{m,n}]
}

// func main() {
//     defer trackTime(time.Now())
//     fmt.Println(gridTravelerMemoized(3,3))
// }