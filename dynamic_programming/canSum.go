// Given a set of N non-negative integers and a target sum S, determine if there exists a subset of the given set such that the sum of the elements of the subset equals to S. Return true if there exists such subset, otherwise return false.

// package dynamic_programming
package main

import (
    "fmt"
)

// O(n^m) Time Complexity, O(m) Space Complexity
func canSumRecursive(target int, numbers []int) bool {
//     fmt.Println("Target is ",target)
    if target == 0 {
        return true
    }

    if target < 0 {
//         fmt.Println("Target less than zero, returning false")
        return false
    }

    for _, n := range numbers {
        remainder := target - n
//         fmt.Println("Loop ", idx)
//         fmt.Println("Remainder: ", remainder)
        if canSumRecursive(remainder, numbers) == true {
            return true
        }
    }

//     fmt.Println("None of the numbers sum to Target")
    return false
}


func canSumMemoized(target int, numbers []int) bool {
    if target == 0 {
        return true
    }
    if target < 0 {
        return false
    }



}

func main() {
    fmt.Println(canSumRecursive(7,[]int{5,3,4,7}))
//     fmt.Println(canSumRecursive(300,[]int{7,14}))
}