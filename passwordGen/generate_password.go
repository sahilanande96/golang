package passwordGen // Package passwordGen contains all the types and functions for generating a random password

import (
	"github.com/sethvargo/go-password/password"
	"log"
)

func CreatePassword() (result string, err error) { // CreatePassword() returns a random password string
	res, err := password.Generate(18, 8, 0, false, false)
	if err != nil {
		log.Fatal(err)
		return "", err
	}
	return res, err
}package passwordGen // Package passwordGen contains all the types and functions for generating a random password

import (
	"github.com/sethvargo/go-password/password"
	"log"
)

func CreatePassword() (result string, err error) { // CreatePassword() returns a random password string
	res, err := password.Generate(18, 8, 0, false, false)
	if err != nil {
		log.Fatal(err)
		return "", err
	}
	return res, err
}
