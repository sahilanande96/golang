package fs

import (
    "io/ioutil"
    "fmt"
    "io"
    "os"
    "bufio"
    "strings"
)

const shortstring string = "This is a short string"
const longstring string = "This is a long string ... \n with multiple characters and I'm crazy that I came up with this"


// This function is used to load the whole text file into memory
func ReadFileWhole() {
    body,err := ioutil.ReadFile("fs/test_file.txt")
    if err != nil {
        fmt.Println("The file cannot be read ", err)
    }
    fmt.Println(string(body))
}

// To read chucks of data from the file
func ReadFileChunk(chunkSize int) {
    f, err := os.Open("fs/test_file.txt")
    if err != nil {
        fmt.Println("The file cannot be read ", err)
    }
    defer f.Close()

    chunk:= make([]byte, chunkSize)
    for {
        n, err := f.Read(chunk)
    	if err == io.EOF {
    		break
    	}
    	if err != nil {
    		fmt.Println(err)
    		continue
    	}
    	if n > 0 {
    		fmt.Println(string(chunk[:n]))
    	}
    }
}

// Read using buffer
func ReadFileBuffer() {
    bufReader := strings.NewReader(shortstring)
    bfSize := bufio.NewReaderSize(bufReader,25)

    fmt.Println("Peek_case1")
    b, err := bfSize.Peek(10)
    if err != nil {
        fmt.Println(err)
    } else {
    fmt.Printf("Peek output is %q\n", b)
    }

    fmt.Println("Peek_case2")
    b, err = bfSize.Peek(50)
    if err != nil {
        fmt.Println(err)
    } else {
    fmt.Printf("Peek output is %q\n", b)
    }

    bfSizeLarge := bufio.NewReaderSize(bufReader,100)
    fmt.Println("Peek_case3")
    b, err = bfSizeLarge.Peek(100)
    if err != nil {
        fmt.Println(err)
    } else {
    fmt.Printf("Peek output is %q\n", b)
    }

    fmt.Println("ReadSlice output",)
    bufReader = strings.NewReader(longstring)
    bf := bufio.NewReader(bufReader)
    readSlice, err := bf.ReadSlice('\n')
    if err != nil {
        fmt.Println(err)
    } else {
    fmt.Printf("%q\n", readSlice)
    }

  fmt.Println("ReadLine")
  bufReader = strings.NewReader(longstring)
  bf = bufio.NewReader(bufReader)
  for {
    token, _ , err := bf.ReadLine()
    if len(token) > 0 {
			fmt.Printf("Token (#): %q\n", token)
		}
		if err != nil {
			break
		}
  }

  fmt.Println("Scanner")
  bufReader = strings.NewReader(longstring)
	scanner := bufio.NewScanner(bufReader)
	for scanner.Scan() {
		fmt.Printf("Token (#): %q\n", scanner.Text())
	}
}
