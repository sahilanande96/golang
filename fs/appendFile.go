package fs

import (
    "os"
    "time"
    "fmt"
)

func AppendFile(fileName string, fileExt string) (File *os.File ,err error) {
    f, err := os.OpenFile(fileName + time.Now().Format("20060102150405") + fileExt, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
    if err != nil {
        fmt.Println(err)
        return nil, err
    }
    return f, err
}