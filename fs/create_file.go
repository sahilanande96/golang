package fs

import (
	"fmt"
	"os"
)

func createFile() {
	f, err := os.Create("test.txt")
	if err != nil {
		fmt.Println("Error creating the file")
		os.Exit(1)
	}

	len_string, err := f.WriteString("A test file is created")
	if err != nil {
		fmt.Println("Error writing in the newly created file")
		os.Exit(1)
	}

	fmt.Printf("The %d characters are written to the file\n", len_string)
	os.Exit(0)
}
