package fs

import (
	"fmt"
	"io/ioutil"
	"k8s.io/apimachinery/pkg/util/yaml"
	"os"
)

// YamlFileToMapInterface converts a yaml file to a map[string]interface{}
func YamlFileToMapInterface(filePath string) map[string]interface{} {
	var result map[string]interface{}
	yamlFile, err := os.Open(filePath)
	if err != nil {
		fmt.Println(err)
	}

	defer yamlFile.Close()

	byteValue, _ := ioutil.ReadAll(yamlFile)

	yaml.Unmarshal(byteValue, &result)
	return result
}
