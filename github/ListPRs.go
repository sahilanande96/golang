package github

import (
	"context"
	"fmt"
	"github.com/google/go-github/github"
	"golang.org/x/oauth2"
	"golang/viper"
)

func ListPR(repoOwner string, repo string, page int) (result []*github.PullRequest, err error) {
	ctx := context.Background()

	ts := oauth2.StaticTokenSource(
		&oauth2.Token{AccessToken: viper.ViperEnvVar("GITHUB_ACCESS_TOKEN")},
	)
	tc := oauth2.NewClient(ctx, ts)

	client := github.NewClient(tc)

	result, _, err = client.PullRequests.List(ctx, repoOwner, repo, &github.PullRequestListOptions{State: "all", ListOptions: github.ListOptions{Page: page, PerPage: 150}})
	if err != nil {
		fmt.Println("Failed to list PullRequest with an error ",err)
	}
	return result, err
}