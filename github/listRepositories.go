package github

import (
    "context"
    "fmt"
    "github.com/google/go-github/github"
    "golang.org/x/oauth2"
    "golang/viper"
)

func listGitHubRepos() (repos []*github.Repository, err error) {
    ctx := context.Background()

    ts := oauth2.StaticTokenSource(
        &oauth2.Token{AccessToken: viper.ViperEnvVar("GITHUB_ACCESS_TOKEN")},
    )
    tc := oauth2.NewClient(ctx, ts)

    client := github.NewClient(tc)

	repos, _, err := client.Repositories.List(ctx, "", nil)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	return repos, nil
}
