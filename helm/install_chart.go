package helm

import (
	"fmt"
	"helm.sh/helm/v3/pkg/action"
	"helm.sh/helm/v3/pkg/chart/loader"
	"helm.sh/helm/v3/pkg/cli"
	"helm.sh/helm/v3/pkg/getter"
	"helm.sh/helm/v3/pkg/repo"
	"k8s.io/cli-runtime/pkg/genericclioptions"
	_ "k8s.io/client-go/plugin/pkg/client/auth"
	"log"
	"os"
)

// NewActionConfig creates a new action.Configuration helm object
func NewActionConfig(releaseNamespace string) (actionConfig *action.Configuration, err error) {
	actionConfig = new(action.Configuration)
	if err := actionConfig.Init(&genericclioptions.ConfigFlags{Namespace: &releaseNamespace}, releaseNamespace, os.Getenv("HELM_DRIVER"), func(format string, v ...interface{}) {
		fmt.Sprintf(format, v)
	}); err != nil {
		var emptyActionConfig *action.Configuration
		return emptyActionConfig, err
	}

	return actionConfig, nil
}

// HelmRepoUpdate performs "helm repo add and helm repo update"
func HelmRepoUpdate(repoName string, repoURL string) {
	var f repo.File
	repoEntry := &repo.Entry{Name: repoName, URL: repoURL}
	// settings contains the path to the directory where to store the repository index file
	r, err := repo.NewChartRepository(repoEntry, getter.All(&cli.EnvSettings{RegistryConfig: "./helm_repo"}))
	if err != nil {
		log.Printf("Helm repo add failed for %s: %s\n", repoName, err)
	}

	_, err = r.DownloadIndexFile()
	if err != nil {
		log.Printf("Failed to fetch the index from %s helm repository with an error %s\n", repoName, err)
	}

	f.Update(repoEntry)
	err = f.WriteFile("./helm_err/"+repoName+"/errFile.txt", 0644)
}

// HelmInstallChart installs a helm chart
func HelmInstallChart(chartPath string, releaseName string, releaseNamespace string, values map[string]interface{}) {

	chart, err := loader.Load(chartPath)
	if err != nil {
		panic(err)
	}

	actionConfig, err := NewActionConfig(releaseNamespace)

	iCli := action.NewInstall(actionConfig)
	iCli.Namespace = releaseNamespace
	iCli.ReleaseName = releaseName
	rel, err := iCli.Run(chart, values)
	if err != nil {
		log.Fatal("Helm Chart Installation Failed for chart %s: %s", releaseName, err)
	}
	log.Println("Successfully installed release: ", rel.Name)
}
