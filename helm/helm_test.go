package helm

import (
	"fmt"
	"golang/fs"
	"testing"
)

func TestHelmRepoUpdate(t *testing.T) {
	repoName := "elastic"
	repoURL := "https://helm.elastic.co"
	HelmRepoUpdate(repoName, repoURL)
}

func TestHelmInstallChart(t *testing.T) {
	releaseName := "es-test-tenant-service"
	releaseNamespace := "default"
	chartPath := "/Users/sahilanande/Library/Caches/helm/repository/elasticsearch-18.2.6.tgz"

	HelmInstallChart(chartPath, releaseName, releaseNamespace, fs.YamlFileToMapInterface("/Users/sahilanande/mendel/infra-services/tenant-service/common/helm/es_values.yaml"))
}

func TestHelmUnInstallChart(t *testing.T) {
	releaseName := "es-test-tenant-service"
	releaseNamespace := "default"

	if rec, err := HelmUnInstallChart(releaseName, releaseNamespace); err != nil {
		fmt.Printf("%v", err)
	} else {
		fmt.Println(rec)
	}
}
