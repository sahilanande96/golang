package helm

import (
	"helm.sh/helm/v3/pkg/action"
	_ "k8s.io/client-go/plugin/pkg/client/auth"
	"log"
)

// HelmUnInstallChart uninstalls a helm chart
func HelmUnInstallChart(releaseName string, releaseNamespace string) (uninstallInfo string, err error) {
	actionConfig, err := NewActionConfig(releaseNamespace)

	iCli := action.NewUninstall(actionConfig)
	rel, err := iCli.Run(releaseName)
	if err != nil {
		log.Fatal("Helm uninstall failed on chart %s: %s", releaseName, err)
		return "", err
	}
	log.Printf("Helm uninstall succeeded on chart %s", releaseName)
	return rel.Info, nil
}
