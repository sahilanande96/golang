func quickSort(arr []int, start int, end int) {
	if start < end {
		pivot := quickSortPartition(arr, start, end)

		quickSort(arr, start, pivot-1)
		quickSort(arr, pivot+1, end)
	}
}

func quickSortPartition(arr []int, start int, end int) int {
	pivot := arr[end]
	hold := start
	for i := start; i < end; i++ {
		if arr[i] <= pivot {
			arr[hold], arr[i] = arr[i], arr[hold]
			hold++
		}
	}
	arr[hold], arr[end] = arr[end], arr[hold]
	return hold
}
