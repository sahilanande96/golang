package main

import (
	"fmt"
	"golang.org/x/exp/constraints"
)


// SelectionSort sorts a slice of integers using the selection sort algorithm
func selectionSort[T constraints.Ordered](listToSort *[]T) {
	for i := 0; i < len(*listToSort)-1; i++ {
		minIdx := i
		// Start from i+1 to find the smallest element in the unsorted part
		for j := i + 1; j < len(*listToSort); j++ {
			if (*listToSort)[minIdx] > (*listToSort)[j] {
				minIdx = j
			}
		}
		// Swap the found minimum element with the first unsorted element
		(*listToSort)[i], (*listToSort)[minIdx] = (*listToSort)[minIdx], (*listToSort)[i]
	}
}
