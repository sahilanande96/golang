package main

import "fmt"

func main() {
	pixelInput := [][]int{
		{1, 1, 1, 1, 1, 1, 1, 1},
		{1, 1, 1, 1, 1, 1, 0, 0},
		{1, 0, 0, 1, 1, 0, 1, 1},
		{1, 2, 2, 2, 2, 0, 1, 0},
		{1, 1, 1, 2, 2, 0, 1, 0},
		{1, 1, 1, 2, 2, 2, 2, 0},
		{1, 1, 1, 1, 1, 2, 1, 1},
		{1, 1, 1, 1, 1, 2, 2, 1},
	}

	floodFill(&pixelInput, 4, 4, pixelInput[4][4], 3)
	for _, pixel := range pixelInput {
		fmt.Println(pixel)
	}

}

func floodFill(pixel *[][]int, x int, y int, oldColor int, newColor int) {
	m := len(*pixel)
	n := len((*pixel)[0])

	if x >= m || y >= n || x < 0 || y < 0 || (*pixel)[x][y] != oldColor || (*pixel)[x][y] == newColor {
		return
	}

	(*pixel)[x][y] = newColor

	floodFill(pixel, x+1, y, oldColor, newColor)
	floodFill(pixel, x-1, y, oldColor, newColor)
	floodFill(pixel, x, y+1, oldColor, newColor)
	floodFill(pixel, x, y-1, oldColor, newColor)
}
