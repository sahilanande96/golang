// Given an array arr[], the task is to find all possible indices {i, j, k} of triplet {arr[i], arr[j], arr[k]} such that their sum is equal to zero and all indices in a triplet should be distinct (i != j, j != k, k != i). We need to return indices of a triplet in sorted order, i.e., i < j < k.

// Examples :

// Input: arr[] = {0, -1, 2, -3, 1}
// Output: {{0, 1, 4}, {2, 3, 4}}
// Explanation:  Two triplets with sum 0 are:
// arr[0] + arr[1] + arr[4] = 0 + (-1) + 1 = 0
// arr[2] + arr[3] + arr[4] = 2 + (-3) + 1 = 0


// Input: arr[] = {1, -2, 1, 0, 5}
// Output: {{0, 1, 2}}
// Explanation: Only triplet which satisfies the condition is arr[0] + arr[1] + arr[2] = 1 + (-2) + 1 = 0

// Input: arr[] = {2, 3, 1, 0, 5}
// Output: {{}}
// Explanation: There is no triplet with sum 0


// With Sorting 
package main

import (
	"fmt"
	"sort"
)

func main() {
	fmt.Println(sumZero([]int{0, -1, 2, -3, 1}))
}

func sumZero(input []int) [][]int {
	
	var result [][]int
	sort.Ints(input)

	for i := 0; i < len(input); i++ {
		l, r := i+1, len(input)-1
		for l < r {
			temp := input[i] + input[l] + input[r]
			if temp == 0 {
				result = append(result, []int{i, l, r})
				l++
			} else if temp > 0 {
				r--
			} else {
				l++
			}

		}
	}
	return result
}

---

// Without Sorting
package main

import (
	"fmt"
	"sort"
)

func main() {
	fmt.Println(sumZero([]int{0, -1, 2, -3, 1}))
}

func sumZero(input []int) [][]int {
	var result [][]int
	for i := 0; i < len(input)-2; i++ {
		if i > 0 && input[i] == input[i-1] {
			continue
		}
		complementMap := make(map[int]int)

		for j := i + 1; j < len(input); j++ {
			required := -input[i] - input[j]

			if _, exists := complementMap[required]; exists {

				triplet := []int{i, j, complementMap[required]}
				sort.Ints(triplet) // Sort indices to meet the requirement i < j < k
				result = append(result, triplet)
			}
			complementMap[input[j]] = j
		}
	}
	return result
}
