package main

import "fmt"

func main() {
	celebrityMatrix := [][]int{{0, 0, 1, 0}, {0, 0, 1, 0}, {0, 0, 0, 0}, {0, 0, 1, 0}}
	fmt.Println(selectCelebrity(celebrityMatrix))

}

func selectCelebrity(celebrityMatrix [][]int) int {
	n := len(celebrityMatrix)
	if n == 0 || len(celebrityMatrix[0]) != n {
		return -1 // Invalid input (non-square matrix)
	}

	i, j := 0, len(celebrityMatrix)-1
	var celebrity int
	for i < j {
		if celebrityMatrix[i][j] == 1 {
			celebrity = j
			i++
		} else {
			j--
		}
	}

	for val := 0; val <= len(celebrityMatrix)-1; val++ {
		if val == celebrity {
			continue
		}

		if celebrityMatrix[val][celebrity] != 1 || celebrityMatrix[celebrity][val] != 0 {
			return -1
		}
	}
	return celebrity
}
