/*
Problem:
- Given a list of stock prices (integer) in chronological order, return the
  max profit from buying at earlier time and selling at later time.

Example:
- Input: []int{10, 7, 5, 8, 11, 9}
  Output: 6, because one can buy at 5 and sell at 11
*/

package practice

func getMaxProfit(sellersBid []int) int {
	minPrice := sellersBid[0]
	maxProfit := sellersBid[1] - sellersBid[0]

	for i := 1; i < len(sellersBid); i++ {
		currentPrice := sellersBid[i]
		potentialProfit := currentPrice - minPrice
		maxProfit = max(maxProfit, potentialProfit)
		minPrice = min(currentPrice, minPrice)
	}
	return maxProfit
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}
