package viper

import (
	"github.com/spf13/viper"
	"log"
)

func ViperEnvVar(key string) string {
	viper.AddConfigPath(".")
	viper.SetConfigName("variables")
	viper.SetConfigType("env")
	viper.AutomaticEnv()

	err := viper.ReadInConfig()
	if err != nil {
		return "Error Reading Viper Config file"
	}

	value, ok := viper.Get(key).(string)

	if !ok {
		log.Fatalf("Invalid type assertion")
	}

	return value
}
