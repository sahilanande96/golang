package main

import (
    "context"
    "os"
    "log"
    "bufio"
    "fmt"
    "strings"
)

type process struct {
    id int
    metadata []string
    fileContent []byte
}

func main () {
     // create a main context, and call cancel at the end, to ensure all our
    // goroutines exit without leaving leaks.
    // Particularly, if this function becomes part of a program with
    // a longer lifetime than this function.

//
//     f, err := os.Open("./itcont_sample_40.txt")
//     if err != nil {
//         log.Fatal(err)
//     }

    batchSize := 10
    numWorkers := 4

    concurrent("./itcont_sample_40.txt", numWorkers, batchSize)


}

func concurrent(file string, numWorkers, batchSize int) {
    ctx, cancel := context.WithCancel(context.Background())
    defer cancel()

    f, err := os.Open(file)
    if err != nil {
        log.Fatal(err)
    }

    reader := func(ctx context.Context, rowsBatch *[]string) <-chan []string {
        out := make(chan []string)

        scanner := bufio.NewScanner(f)

        go func() {
            defer close(out) // close channel when we are done sending all rows

            for {
                scanned := scanner.Scan()

                select {
                case <-ctx.Done():
                    return
                default:
                    row := scanner.Text()
                    // if batch size is complete or end of file, send batch out
                    if len(*rowsBatch) == batchSize || !scanned {
                        out <- *rowsBatch
                        *rowsBatch = []string{} // clear batch
                    }
                    *rowsBatch = append(*rowsBatch, row) // add row to current batch
                }

                // if nothing else to scan return
                if !scanned {
                    return
                }
            }
        }()

        return out

    }


    worker:= func (ctx context.Context, rowBatch <-chan []string) <-chan bool {
        out := make(chan bool)

        go func() {
            defer close(out)
            for rowBatch := range rowBatch {
                for _, row := range rowBatch {
//                     fmt.Println("Row contains ", row)
                    s := strings.Split(row, "|")
                    f, err := os.OpenFile("./target/" + s[4] + ".txt", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
                    if err != nil {
                      panic(err) // i'm simplifying it here. you can do whatever you want.
                    }
                    defer f.Close()
                    f.WriteString(s[7])
//                     fmt.Println("First index",s[0])
//                     fmt.Println("Last index",s[1])
                }
            }
            out <- true
        }()

        return out
    }

    // STAGE 1: start reader
    rowsBatch := []string{}
    rowsCh := reader(ctx, &rowsBatch)

    // STAGE 2: create a slice of processed output channels with size of numWorkers
    // and assign each slot with the out channel from each worker.
    workersCh := make([]<-chan bool, numWorkers)
    for i := 0; i < numWorkers; i++ {
        workersCh[i] = worker(ctx, rowsCh)

            for j := range workersCh[i]{
                fmt.Println(j)
            }
    }

}

