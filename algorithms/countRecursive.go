package algorithm

import(
	"golang.org/x/exp/constraints"
)

func countRecursive[T any](listToCount []T) int {
	if len(listToCount) == 0 {
		return 0
	}
	return 1 + countRecursive(listToCount[1:])
}
