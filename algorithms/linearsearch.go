package algorithms

import (

)

func LinearSearch(arr []int, key int ) bool {
    if len(arr) == 0 {
        return false
    }

    for _, item := range arr {
        if item == key {
            return true
        }
    }

    return false
}
