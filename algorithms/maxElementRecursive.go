package algorithm

import(
	"golang.org/x/exp/constraints"
)

func maxElementRecursive[T constraints.Ordered](maxElementList []T) T {
	if len(maxElementList) == 1 {
		return maxElementList[0]
	}
	maxElementTemp := maxElementRecursive(maxElementList[1:])

	if maxElementList[0] > maxElementTemp {
		return maxElementList[0]
	}
	return maxElementTemp
}
