package algorithm

import (
	"fmt"
	"golang.org/x/exp/constraints"
)

func binarySearch[T constraints.Ordered](listToSearch []T, searchParam T) (T, error) {
	var result T
	head, tail := 0, len(listToSearch)-1

	for head <= tail {
		mid := (head + tail) / 2
		if listToSearch[mid] == searchParam {
			return listToSearch[mid], nil
		} else if listToSearch[mid] > searchParam {
			tail = mid - 1
		} else {
			head = mid + 1
		}
	}

	return result, fmt.Errorf("The searchParam doesn't exist in the list")
}

