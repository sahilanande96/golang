package algorithm

import (
	"golang.org/x/exp/constraints"
)

func sumRecursion[T constraints.Ordered](listToSum []T) T {
	var sum T
	if len(listToSum) == 0 {
		return sum
	}
	sum = listToSum[0] + sumRecursion(listToSum[1:])
	return sum
}
