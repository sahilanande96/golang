package algorithms

import(
	"fmt"
	"golang.org/x/exp/constraints"
)

func sumRecursion[T constraints.Ordered](listToAdd []T) T {
	var sum T
	if len(listToAdd) == 0 {
		return sum
	}

	sum = listToAdd[0] + sumRecursion(listToAdd[1:])
	return sum
}